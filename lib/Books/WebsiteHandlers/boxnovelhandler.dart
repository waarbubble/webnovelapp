import 'package:intl/intl.dart';

import 'WebsiteHandler.dart';
import 'package:webnovel_app/Books/Book/about.dart';
import 'package:webnovel_app/Books/Book/chapter.dart';

class BoxNovelHandler extends WebsiteHandler {
  BoxNovelHandler(String url) {
    this.setUrl(url);
  }

  Future<String> fetchHomeRaw() async {
    final response = await WebsiteHandler.fetch("https://boxnovel.com/");
    if (response.statusCode == 200) {
      return response.body;
    } else {
      throw Exception("failed to get data");
    }
  }

  /// brief: Checks if current url is an about page
  /// return: true if the url is an about page
  bool isAboutPage() {
    if (RegExp(r"boxnovel\.com/novel/.*").hasMatch(this.getUrl())) {
      if (RegExp(r"boxnovel\.com/novel/.*/chapter-[0-9]*/?")
          .hasMatch(this.getUrl())) {
        return false;
      } else {
        return true;
      }
    } else {
      return false;
    }
  }

  /// brief: derive the aboutpage url from the current url
  /// return: about page url
  /// exception: about page url can't be derived from url
  String getAboutUrl() {
    String url = this.url;
    if (!this.isAboutPage()) {
      if (RegExp(r"boxnovel\.com/.*/chapter.*").hasMatch(url)) {
        url = url.replaceAll(RegExp(r"/chapter-[0-9]*/?$"), "");
      } else {
        throw Exception("Can't derive aboutpage from url");
      }
    }
    return url;
  }

  /// brief: get the about page of a book
  /// return: info about the book
  /// exception: about page url can't be derived from url
  /// exception: can't find book title
  Future<AboutBook> getAboutPage() async {
    String url = this.getAboutUrl();
    final response = await WebsiteHandler.fetch(url);

    //Find start of about section
    String body = response.body
        .replaceFirst(RegExp(r"<head>.*?</head>", dotAll: true), "")
        .replaceAll(RegExp(r"<script.*?</script>", dotAll: true), "");

    //Get Title
    String? title = RegExp(r'<h3>.*?(?=</h3>)', dotAll: true).stringMatch(body);
    if (title == null) throw Exception("Can't find book title");
    title = title.replaceAll(RegExp(r"<.*>"), "").trim();

    //Get Auther
    String? author =
        RegExp(r'<div class="author-content">.*?(?=</div>)', dotAll: true)
            .stringMatch(body);
    if (author == null) author = "";
    author = author.replaceAll(RegExp(r"<.*?>"), "").trim();

    //get Genres
    String? genres =
        RegExp(r'<div class="genres-content">.*?(?=</div>)', dotAll: true)
            .stringMatch(body);
    if (genres == null) genres = "";
    genres = genres.replaceAll(RegExp(r"<.*?>"), "").trim();

    //get Summary
    String? summary = RegExp(
            r'<div class="summary__content show-more">.*?(?=</div>)',
            dotAll: true)
        .stringMatch(body);
    if (summary == null) summary = "";
    summary = summary
        .replaceAll("<h4 class=\"seriesinfo\">Description</h4>", "")
        .replaceAll(RegExp(r"<.*?>"), "")
        .split(RegExp(r"[Yy]ou(&#8217;|')re reading Chinese web Novel"))[0]
        .replaceAll("\n", "")
        .trim()
        .replaceFirst(RegExp(r"\.[_\s]*?$"), ".");

    //get Image
    String? imageUrl =
        RegExp(r'<div class="summary_image">.*?(?=</div>)', dotAll: true)
            .stringMatch(body);
    if (imageUrl == null) imageUrl = "";
    imageUrl = RegExp(r'src=".*?"')
        .stringMatch(imageUrl)!
        .replaceAll("src=", "")
        .replaceAll("\"", "");

    return AboutBook(title,
        image_url: imageUrl,
        about: summary,
        author: author,
        genres: genres.split(","));
  }

  /// Checks if current url is a chapter page
  bool isChapterPage({String? url}) {
    if (url == null) url = this.url;
    if (RegExp(r"chapter-[0-9]+/?$").hasMatch(url)) {
      return true;
    }
    return false;
  }

  /// brief: get Chapter text
  Future<String> getChapterText() async {
    if (!this.isChapterPage()) {
      throw Exception("url is not a chapter");
    }

    var response = await WebsiteHandler.fetch(this.url);

    String body = RegExp(r'<div class="text-left">.*?<footer', dotAll: true)
        .stringMatch(response.body)!;
    String chapterText = "";
    bool first = true;
    for (RegExpMatch para in RegExp(r"<p>.*?</p>").allMatches(body)) {
      if (first) {
        first = false;
      } else {
        String paragraph = para.group(0)!;
        chapterText += paragraph.replaceAll(RegExp(r"</?p>"), "") + "\n";
      }
    }
    return chapterText;
  }

  /// brief: get Chapter text
  Future<List<Chapter>> getChapterList() async {
    String url = this.getAboutUrl();
    final response = await WebsiteHandler.fetch(url);
    String body = response.body.substring(
        response.body.indexOf(RegExp(r'<li class="wp-manga-chapter">')),
        response.body.indexOf(RegExp(r'<div class="c-chapter-readmore">')));

    List<String> ch_string = body.split(RegExp(r"</li>"));
    ch_string.removeLast();

    List<Chapter> chapters = [];
    for (var ch in ch_string) {
      String link =
          RegExp(r'https://.*?">').stringMatch(ch)!.replaceFirst('">', "");

      String title = RegExp(r'<a.*?>.*?</a>', dotAll: true)
          .stringMatch(ch)!
          .replaceAll(RegExp(r'</?a.*?>'), "")
          .replaceAll('"', "")
          .trim();
      //Chapter 2171 – - Sorrowful Silence’s Shock
      String test = RegExp(r'Chapter [0-9.]+?(\s+[-–])?').stringMatch(title)!;
      List<String> chap_nr = RegExp(r'Chapter [0-9.]+?(\s+[-–])?')
          .stringMatch(title)!
          .replaceAll(RegExp("[^0-9.]"), "")
          .trim()
          .split(".");
      ChapterID nr = new ChapterID();
      nr.chapter = int.parse(chap_nr[0]);
      if (chap_nr.length == 2) {
        nr.chapter = int.parse(chap_nr[1]);
      }

      title = title.replaceFirst(RegExp(r'Chapter.*?-'), "").trim();

      String date_s = RegExp(r'<i>.*?</i>', dotAll: true)
          .stringMatch(ch)!
          .replaceAll(RegExp(r'</?i>'), "")
          .trim();

      DateTime date;
      if (date_s.contains("ago")) {
        date = DateTime.now();
        if (date_s.contains(RegExp(r"days?"))) {
          date.subtract(Duration(
              days: int.parse(date_s.replaceAll(RegExp(r"days? ago"), ""))));
        } else if (date_s.contains("hours")) {
          date.subtract(
              Duration(hours: int.parse(date_s.replaceAll("hours ago", ""))));
        } else if (date_s.contains("min")) {
          date.subtract(
              Duration(minutes: int.parse(date_s.replaceAll("min ago", ""))));
        } else if (date_s.contains("seconds")) {
          date.subtract(Duration(
              seconds: int.parse(date_s.replaceAll("seconds ago", ""))));
        } else {
          throw Exception("date not recocgnized: " + date_s);
        }
      } else {
        date = DateFormat("MMMM d, yyyy", 'en_US').parse(date_s.trim());
      }

      chapters.add(new Chapter(title, WebsiteHandler.getHandler(link), nr,
          relaseDate: date));
      /*print(title);
      print(nr);
      print(link);
      print(date_s);
      print(date);*/
    }
    chapters = new List.from(chapters.reversed);

    for (var i = 1; i < chapters.length; i++) {
      chapters[i].setPreviousChapter(chapters[i - 1]);
    }

    return chapters;
  }

  /// brief: get next Chapter from url
  /// url: the current Chapter you want to know the next Chapter of
  /// return: websitehandler of the next Chapter
  Future<WebsiteHandler> getNextChapter({String? url}) async {
    if (url == null) url = this.url;
    if (!this.isChapterPage(url: url)) {
      throw Exception("url is not a chapter");
    }
    var response = await WebsiteHandler.fetch(this.url);

    RegExpMatch match = RegExp(r'<a href="(.*?)" class="btn next_page"')
        .firstMatch(response.body)!;
    if (match == null) {
      throw Exception("next chapter doesn't exsist");
    }

    return WebsiteHandler.getHandler(match.group(1)!);
  }

  /// brief: get previous Chapter from url
  /// url: the current Chapter you want to know the previous Chapter of
  /// return: websitehandler of the previous Chapter
  Future<WebsiteHandler> getPreviousChapter({String? url}) async {
    if (url == null) url = this.url;
    if (!this.isChapterPage(url: url)) {
      throw Exception("url is not a chapter");
    }
    var response = await WebsiteHandler.fetch(this.url);

    RegExpMatch match = RegExp(r'<a href="(.*?)" class="btn prev_page"')
        .firstMatch(response.body)!;
    if (match == null) {
      throw Exception("next chapter doesn't exsist");
    }

    return WebsiteHandler.getHandler(match.group(1)!);
  }
}
