import 'package:http/http.dart' as http;
import 'package:webnovel_app/Books/Book/about.dart';
import 'package:webnovel_app/Books/Book/chapter.dart';
import 'package:webnovel_app/Books/WebsiteHandlers/boxnovelhandler.dart';

abstract class WebsiteHandler {
  String url = "";

  String getUrl() {
    return this.url;
  }

  void setUrl(String url) {
    this.url = url;
  }

  static Future<http.Response> fetch(String url) async {
    var uri = Uri.parse(url);
    http.Response response = await http.get(uri);
    return response;
  }

  Future<String> fetchHome_raw() async {
    //TODO get the core part of the url
    final response = await WebsiteHandler.fetch(url);
    if (response.statusCode == 200) {
      return response.body;
    } else {
      throw Exception("failed to get data");
    }
  }

  /// Checks if current url is an about page
  bool isAboutPage();

  /// brief: derive the aboutpage url from the current url
  /// return: about page url
  /// exception: about page url can't be derived from url
  String getAboutUrl();

  /// brief: get the about page of a book
  /// return: info about the book
  /// exception: about page url can't be derived from url
  Future<AboutBook> getAboutPage();

  /// Checks if current url is a chapter page
  bool isChapterPage({String url});

  /// brief: get a list of Chapter
  Future<List<Chapter>> getChapterList();

  /// brief: get Chapter text
  Future<String> getChapterText();

  /// brief: get next Chapter from url
  /// url: the current Chapter you want to know the next Chapter of
  /// return: websitehandler of the next Chapter
  Future<WebsiteHandler> getNextChapter({String url});

  /// brief: get previous Chapter from url
  /// url: the current Chapter you want to know the previous Chapter of
  /// return: websitehandler of the previous Chapter
  Future<WebsiteHandler> getPreviousChapter({String url});

  static WebsiteHandler getHandler(String url) {
    if (RegExp(r"boxnovel\.com").hasMatch(url)) {
      return BoxNovelHandler(url);
    } else {
      throw Exception("Can't find any handler for this domain");
    }
  }
}
