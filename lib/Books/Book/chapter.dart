import 'package:flutter/rendering.dart';

import 'package:webnovel_app/Books/WebsiteHandlers/WebsiteHandler.dart';
import 'page.dart';

class ChapterID {
  int volume = 0;
  int chapter = 0;
  int part = 0;

  @override
  String toString() {
    if (this.part > 0) {
      return "Vol. " +
          this.volume.toString() +
          " ch. " +
          this.chapter.toString();
    } else {
      return "Vol. " +
          this.volume.toString() +
          " ch. " +
          this.chapter.toString() +
          "." +
          this.part.toString();
    }
  }
}

class Chapter {
  Chapter? prev;
  Chapter? next;
  String title;
  ChapterID chapter;
  WebsiteHandler webHandler;
  DateTime? relaseDate;
  String text;
  List<Page> pages;
  int currentPage;
  bool isInit = false;

  Chapter(this.title, this.webHandler, this.chapter,
      {this.prev, this.next, this.relaseDate})
      : text = "",
        currentPage = 0,
        pages = <Page>[] {
    if (relaseDate == null) {
      relaseDate = DateTime.fromMillisecondsSinceEpoch(0);
    }
  }

  /// get the Titel of the book
  String getTitel() {
    return this.title;
  }

  /// Initialize this chaptor.
  /// This will download the chaptor text and seperate it out into pages
  Future<void> init() async {
    if (!this.isInit) {
      this.text = await this.webHandler.getChapterText();

      // Create a virtual page and count lines
      TextSpan span =
          new TextSpan(text: this.text, style: PageSettings.pageTextStyle);
      TextPainter tp = new TextPainter(
          text: span,
          textAlign: TextAlign.left,
          textDirection: TextDirection.ltr);
      tp.layout(maxWidth: PageSettings.pageWidth);
      TextSelection selection =
          TextSelection(baseOffset: 0, extentOffset: this.text.length);
      List<TextBox> boxes = tp.getBoxesForSelection(selection);

      // Do the same for titel
      TextSpan titelSpan =
          new TextSpan(text: this.title, style: PageSettings.titleTextStyle);
      TextPainter titleTp = new TextPainter(
          text: titelSpan,
          textAlign: TextAlign.left,
          textDirection: TextDirection.ltr);
      titleTp.layout(maxWidth: PageSettings.pageWidth);
      TextSelection titleSelection =
          TextSelection(baseOffset: 0, extentOffset: this.title.length);
      List<TextBox> titleBoxes = titleTp.getBoxesForSelection(titleSelection);

      //result
      int numberOfLines = boxes.length;
      ((numberOfLines + PageSettings.linesForTitle * titleBoxes.length) /
              (PageSettings.linesPerPage))
          .ceil();

      this.pages = [];
      int start_index = 0;
      while (start_index < this.text.length) {
        for (var i = this.text.length; i > start_index; i--) {
          TextSelection selection =
              TextSelection(baseOffset: start_index, extentOffset: i);
          List<TextBox> boxes = tp.getBoxesForSelection(selection);
          int lpp = PageSettings.linesPerPage;
          if (this.pages.isEmpty) {
            lpp -= PageSettings.linesForTitle * titleBoxes.length;
          }
          if (boxes.length <= lpp) {
            this.pages.add(new Page(this.text.substring(start_index, i)));
            start_index = i;
            break;
          }
        }
      }
    }
    this.isInit = true;
  }

  /// get the whole text of the chaptor
  /// Exception: an exception is thrown if init has not been run
  String getBody() {
    if (!this.isInit) {
      throw Exception("Chapter is not initialized");
    }
    return this.text;
  }

  /// get a specific page
  /// Exception: an exception is thrown if init has not been run
  /// Exception: an exception is thrown if index is out of bound
  Page getPage(int pageNumber) {
    if (!this.isInit) {
      throw Exception("Chapter is not initialized");
    }
    return this.pages[pageNumber];
  }

  /// returns the number of pages
  /// Exception: an exception is thrown if init has not been run
  int pageCount() {
    if (!this.isInit) {
      throw Exception("Chapter is not initialized");
    }
    return this.pages.length;
  }

  int getCurrentPageNum() {
    return this.currentPage;
  }

  /// get the url for the current Chapter
  String getCurrentChapterUrl() {
    return this.webHandler.getUrl();
  }

  /// get url for the next Chapter of the book
  Chapter getNextChapter() {
    if (!this.isInit || this.next == null) {
      throw Exception("Chapter is not initialized");
    }
    return this.next!;
  }

  /// get url for previus Chapter of the book
  Chapter getPreviousChapter() {
    if (!this.isInit || this.prev == null) {
      throw Exception("Chapter is not initialized");
    }
    return this.prev!;
  }

  void setPreviousChapter(Chapter prev) {
    if (!this.isInit || this.prev == null) {
      throw Exception("Chapter is not initialized");
    }
    this.prev = prev;
    this.prev!.next = this;
  }
}
