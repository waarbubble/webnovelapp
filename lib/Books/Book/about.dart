import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class AboutBook {
  List<String> genres = [];
  Image image;
  String title;
  String about;
  double rating;
  String author;

  AboutBook(this.title,
      {this.image = const Image(image: AssetImage('assets/imges/Logo.png')),
      image_url,
      this.about = "",
      this.rating = 0,
      this.author = "unknown",
      this.genres = const <String>[]}) {
    if (image_url == null) {
      this.image = new Image(image: AssetImage('assets/imges/Logo.png'));
    } else {
      this.image = Image.network(image_url);
    }
  }

  @override
  String toString() {
    return "AboutBook: \n\tTitle: ${title}\n\tAuthor: ${author}\n\tGenres: ${genres}\n\tRating: ${rating}\n\tSummary: ${about}";
  }
}
