import 'about.dart';
import 'chapter.dart';
import 'package:mutex/mutex.dart';
import 'package:webnovel_app/Books/WebsiteHandlers/WebsiteHandler.dart';

class Book {
  AboutBook? about;
  Chapter? currentChapter;
  String? url;
  List<Chapter> chapterList;

  bool ready = false;
  Mutex initMutex = Mutex();

  Book({String? bookurl}) : chapterList = <Chapter>[] {
    if (bookurl != null) this.init(bookurl: bookurl);
  }

  /// Brief: download the content of the book
  Future<void> init({String? bookurl}) async {
    await initMutex.acquire();
    if (!this.ready) {
      String url = "";
      if (bookurl == null) {
        if (this.url == null) {
          throw Exception(
              "Either the provided url or the class url cannot be null");
        } else {
          url = this.url.toString();
        }
      } else {
        url = bookurl;
      }
      var handler = WebsiteHandler.getHandler(url);
      this.about = await handler.getAboutPage();
      this.chapterList = await handler.getChapterList();

      if (handler.isChapterPage()) {
        for (Chapter ch in this.chapterList) {
          if (ch.getCurrentChapterUrl() == handler.getUrl()) {
            currentChapter = ch;
          }
        }
      } else {
        currentChapter = this.chapterList[0];
      }
      if (!(currentChapter == null)) {
        await currentChapter!.init();
      }
      ready = true;
    }
    initMutex.release();
  }

  /// Brief: get information about the book
  AboutBook getAbout() {
    if (!ready || this.about == null) {
      throw Exception("Book is not initialized");
    }
    return this.about!;
  }

  /// Brief: set the current chapter of the book to save progress
  ///        and preload more chapters
  void setCurrentChapter(Chapter chap) {
    this.currentChapter = chap;
  }

  /// Brief: get the current chapter
  Chapter getCurrentChapter() {
    if (!ready || this.currentChapter == null) {
      throw Exception("Book is not initialized");
    }
    return this.currentChapter!;
  }

  String getPageString(int nr) {
    int page = -1;
    while (page < nr) {}
    return "text";
  }

  int pageCount() {
    return 0;
  }
}
