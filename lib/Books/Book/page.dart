import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class PageSettings {
  static TextStyle titleTextStyle = TextStyle(
      fontSize: 24,
      color: Colors.black,
      fontWeight: FontWeight.bold,
      height: 1.2);
  static TextStyle pageTextStyle =
      TextStyle(fontSize: 16, color: Colors.black, height: 1.2);
  static int linesPerPage = 20;
  static int linesForTitle = 2;
  static double pageWidth = 400;
  static double pageMargin = 5;
  static double pageHeight = 400;
  static void UpdatePageSettings({double? width, double? height}) {
    if (width != null) {
      PageSettings.pageWidth = width * 0.9;
      PageSettings.pageMargin = (width - PageSettings.pageWidth) / 2;
    }
    if (height != null) {
      PageSettings.pageHeight = height;
    }
    double lineHeight =
        PageSettings.pageTextStyle.fontSize! * PageSettings.pageTextStyle.height!;

    PageSettings.linesForTitle = ((PageSettings.titleTextStyle.fontSize! *
                PageSettings.titleTextStyle.height! /
                lineHeight) +
            1)
        .toInt();
    PageSettings.linesPerPage = (PageSettings.pageHeight ~/ lineHeight) - 1;
  }
}

class Page {
  int pos;
  String text;

  Page(this.text, {this.pos=0});
}
