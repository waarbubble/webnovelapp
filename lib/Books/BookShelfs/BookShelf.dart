import 'package:webnovel_app/Books/Book/book.dart';

abstract class BookShelf {
  /// brief: add a book to the shelf
  /// book: the book to add to the shelf
  /// return: true if succesful
  bool addBook(Book book);

  /// brief: sort the shelf depending on the use case
  void sortShelf();

  /// brief: get the book a specifc spot
  /// booknr the spot of the book
  ///
  Book getBook(int booknr);

  /// brief: get the amount of books stored on the shelf
  /// return: the number of bugs or -1 if infinate long
  int size();
}
