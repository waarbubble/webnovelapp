import 'package:webnovel_app/Books/Book/book.dart';
import 'package:webnovel_app/Books/BookShelfs/BookShelf.dart';

class ReadNext extends BookShelf {
  List<Book> booklist;

  ReadNext() : booklist = <Book>[];

  /// brief: add a book to the shelf
  /// book: the book to add to the shelf
  /// return: true if succesful
  bool addBook(Book book) {
    if (!this.booklist.contains(book)) {
      if (!book.ready) {
        booklist.add(book);
        return true;
      }
    }
    return false;
  }

  /// brief: sort the shelf depending on the use case
  void sortShelf() {}

  /// brief: get the book a specifc spot
  /// booknr the spot of the book
  /// return the book wanted book
  Book getBook(int booknr) {
    assert(booknr < booklist.length);
    return booklist[booknr];
  }

  /// brief: get the amount of books stored on the shelf
  /// return: the number of bugs or -1 if infinate long
  int size() {
    return booklist.length;
  }
}
