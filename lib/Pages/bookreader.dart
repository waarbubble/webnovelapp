import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:webnovel_app/Books/Book/page.dart';
import 'package:webnovel_app/Pages/PageWidget.dart';
import 'package:loading_overlay/loading_overlay.dart';

import 'package:webnovel_app/Books/Book/chapter.dart';
import 'package:webnovel_app/Books/Book/book.dart';

class BookReader extends StatefulWidget {
  Book currentBook;
  Chapter? currentChap;
  int currentPage = 0;

  @override
  BookReader(this.currentBook) {
    if (this.currentBook.ready) {
      currentChap = currentBook.getCurrentChapter();
    }
  }
  @override
  _BookReaderState createState() => _BookReaderState();
}

class _BookReaderState extends State<BookReader> {
  final PageController bookPageController = PageController(initialPage: 0);
  @override
  Widget build(BuildContext context) {
    // Wait for book to load
    if (this.widget.currentBook.ready == false) {
      this.widget.currentBook.init().then((value) {
        setState(() {
          this.widget.currentChap = this.widget.currentBook.getCurrentChapter();
        });
      });
      return Scaffold(
          appBar: AppBar(
              title: Text(
            'BookName',
          )),
          body: LoadingOverlay(
            isLoading: true,
            child: Text(""),
          ));
    }

    // ***************
    // * Render book *
    // ***************

    // Load chapter pages
    List<Widget> pagesList = [];
    for (int i = 0; i < this.widget.currentChap!.pageCount(); i++) {
      if (i == 0) {
        pagesList.add( new TitlePageWidget(this.widget.currentChap!.getTitel(),
                this.widget.currentChap!.getPage(i).text));
      } else {
        pagesList.add(
            new PageWidget(this.widget.currentChap!.getPage(i).text));
      }
    }
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'BookName',
        ),
        actions: [],
      ),
      body: PageView(
        controller: bookPageController,
        children: pagesList,
      ),
    );
  }
}
