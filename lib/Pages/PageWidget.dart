import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:webnovel_app/Books/Book/page.dart';

class PageWidget extends StatefulWidget {
  final String pageText;
  @override
  PageWidget(this.pageText);
  @override
  _PageWidgetState createState() => _PageWidgetState();
}

class _PageWidgetState extends State<PageWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.symmetric(horizontal: PageSettings.pageMargin),
        alignment: Alignment.center,
        child: Text(this.widget.pageText, style: PageSettings.pageTextStyle));
  }
}

class TitlePageWidget extends StatefulWidget {
  final String pageText;
  final String chapterText;
  @override
  TitlePageWidget(this.chapterText, this.pageText);
  @override
  _TitlePageWidgetState createState() => _TitlePageWidgetState();
}

class _TitlePageWidgetState extends State<TitlePageWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: PageSettings.pageMargin),
      alignment: Alignment.center,
      child: Column(children: [
        Text(this.widget.chapterText, style: PageSettings.titleTextStyle),
        Text(
          this.widget.pageText,
          style: PageSettings.pageTextStyle,
        )
      ]),
    );
  }
}
