import 'package:webnovel_app/Books/Book/book.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:webnovel_app/Books/Book/chapter.dart';
import 'package:webnovel_app/Books/Book/page.dart';
import 'package:webnovel_app/Books/BookShelfs/ReadNext.dart';
import 'bookreader.dart';

class HomePage extends StatefulWidget {
  ReadNext topBookList = new ReadNext();

  HomePage() {
    topBookList.addBook(Book(
        bookurl:
            "https://www.boxnovel.com/novel/reincarnation-of-the-strongest-sword-god-boxnovel/chapter-2807"));
    topBookList.addBook(Book(
        bookurl:
            "https://www.boxnovel.com/novel/koko-wa-ore-ni-makasete-saki-ni-ike-to-itte-kara-10-nen-ga-tattara-densetsu-ni-natteita/chapter-140"));
    topBookList.addBook(Book(
        bookurl:
            "https://www.boxnovel.com/novel/the-legendary-mechanic-boxnovel/chapter-1378"));
    topBookList.addBook(Book(
        bookurl:
            "https://www.boxnovel.com/novel/versatile-mage-boxnovel/chapter-2008"));
  }
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    AppBar appBar = AppBar(title: Text("Visual Novel Reader"));
    PageSettings.UpdatePageSettings(
        width: MediaQuery.of(context).size.width,
        height:
            MediaQuery.of(context).size.height - appBar.preferredSize.height);
    return Scaffold(
      appBar: appBar,
      body: Column(
        children: [
          Text('Latest updates'),
          Container(
            height: 200,
            child: CustomScrollView(
              scrollDirection: Axis.horizontal,
              slivers: <Widget>[
                SliverFixedExtentList(
                  itemExtent: 200,
                  delegate: SliverChildListDelegate([
                    //Navigator.push(context, MaterialPageRoute(builder: (context) => Level1()));
                    Container(
                      height: 100,
                      width: 100,
                      color: Colors.red,
                      padding: EdgeInsets.all(10),
                      child: OutlineButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => BookReader(new Book(
                                      bookurl:
                                          "https://boxnovel.com/novel/reincarnation-of-the-strongest-sword-god-boxnovel/chapter-2807"))));
                        },
                      ),
                    ),
                    Container(
                      height: 100,
                      width: 100,
                      color: Colors.blueAccent,
                      padding: EdgeInsets.all(10),
                    ),
                  ]),
                )
              ],
            ),
          ),
          OutlineButton(
            onPressed: () {
              Book testbook = new Book();
              testbook
                  .init(
                      bookurl:
                          "https://boxnovel.com/novel/reincarnation-of-the-strongest-sword-god-boxnovel/chapter-2807")
                  .then((value) {
                Chapter chap = testbook.getCurrentChapter();
                print(chap.isInit);
                print(chap.chapter);
                print(chap.pageCount());
                print(chap.getPage(0).text);
              });
            },
            child: Text('kasper teste btn'),
          )
        ],
      ),
      //Center(child: Text('text')),

      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Container(
                decoration: new BoxDecoration(
                  image: new DecorationImage(
                    image: new AssetImage("assets/images/Logo.png"),
                    fit: BoxFit.contain,
                  ),
                ),
                margin: EdgeInsets.fromLTRB(0, 5, 0, 0),
              ),
              decoration: new BoxDecoration(color: Colors.blue),
            ),
            ListTile(
              title: Text('ThemeSwitch'),
              onTap: () {},
            ),
            ListTile(
              title: Text('Library'),
              onTap: () {},
            ),
            ListTile(
              title: Text('other'),
              onTap: () {},
            )
          ],
        ),
      ),
    );
  }
}
